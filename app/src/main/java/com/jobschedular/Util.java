package com.jobschedular;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by Malti on 2/25/2018.
 */

public class Util {
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static  void  scheduleJob(Context context){
        ComponentName componentName=new ComponentName(context,JobService.class);
        JobInfo.Builder builder=new JobInfo.Builder(1,componentName);
        builder.setMinimumLatency(2*2000);
       builder.setRequiresCharging(true);
//        builder.setOverrideDeadline(3*1000);
        JobScheduler jobScheduler=context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());

    }
}
